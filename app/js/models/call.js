(() => {
  'use strict';

  angular
    .module('angular-ac')
    .factory('Call', ($http) => {
      const api = 'https://aircall-job.herokuapp.com';
      const calls = `${api}/activities`;
      const reset = `${api}/reset`;
      const _call = {};

      _call.getAll = () => {
        return $http.get(calls);
      }

      _call.getDetail = id => {
        return $http.get(`${calls}/${id}`);
      }

      _call.archiveToggle = call => {
        return $http.post(`${calls}/${call.id}`, { is_archived: !call.is_archived });
      }

      _call.archive = id => {
        return $http.post(`${calls}/${id}`, { is_archived: true });
      }

      _call.unarchiveAll = () => {
        return $http.get(reset);
      }

      return _call;
    });

})();
