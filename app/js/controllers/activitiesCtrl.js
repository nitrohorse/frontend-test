(() => {
  'use strict';

  angular
    .module('angular-ac')
    .controller('ActivitiesController', ($scope, $state, Call) => {
      console.info('[ActivitiesController] init');

      const init = () => {
        Call.getAll().then(response => {
          $scope.calls = response.data;
        });
      }

      $scope.goToDetail = id => {
        $state.go('activity_detail', { id });
      }

      $scope.archive = call => {
        Call.archive(call.id).then(response => {
          call.is_archived = response.data.is_archived; // hide from view
        });
      }

      $scope.unarchiveAll = () => {
        Call.unarchiveAll().then(response => {
          $scope.calls.forEach(call => {
            call.is_archived = false; // show in view
          });
        });
      }

      init();
    });

})();
