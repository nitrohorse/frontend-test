(() => {
  'use strict';

  angular
    .module('angular-ac', [
      'angular.filter',
      'ui.router',
      'ngAnimate'
    ]);

})();
