(() => {
  'use strict';

  angular
    .module('angular-ac')
    .controller('AppController', () => {
      console.info('[AppController] init');
    });

})();
