(() => {
  'use strict';

  angular
    .module('angular-ac')
    .controller('ActivityDetailController', ($scope, $state, $stateParams, Call) => {
      console.info('[ActivityDetailController] init');

      const init = () => {
        Call.getDetail($stateParams.id).then(response => {
          $scope.call = response.data;
        });
      }

      $scope.archiveToggle = call => {
        Call.archiveToggle(call).then(response => {
          call.is_archived = response.data.is_archived;
        });
      }

      $scope.goToAllCalls = () => {
        $state.go('activities');
      }

      init();
    });

})();
